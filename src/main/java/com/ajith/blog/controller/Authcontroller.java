package com.ajith.blog.controller;

import com.ajith.blog.dto.LoginRequest;
import com.ajith.blog.dto.RegisterRequest;
import com.ajith.blog.service.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class Authcontroller {

    @Autowired
    private AuthService autheAuthService;

    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody RegisterRequest registerRequest) {
        autheAuthService.signup(registerRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/login")
    public  String login(@RequestBody LoginRequest loginRequest) {
        return autheAuthService.login(loginRequest);
    }
    
}
