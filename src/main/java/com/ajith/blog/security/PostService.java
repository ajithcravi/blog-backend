package com.ajith.blog.security;

import java.time.Instant;
import java.util.List;
import static java.util.stream.Collectors.toList;

import com.ajith.blog.dto.PostDto;
import com.ajith.blog.model.Post;
import com.ajith.blog.repository.PostRepository;
import com.ajith.blog.service.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
public class PostService {

    @Autowired
    private AuthService authService;

    @Autowired
    private PostRepository postRepository;

    public void createPost(PostDto postDto) {
        User username = authService.getCurrentUser().orElseThrow(() -> new IllegalArgumentException("No user logged in"));
        Post post = new Post();
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setUsername(username.getUsername());
        post.setCreatedOn(Instant.now());

        postRepository.save(post);
    }

    public List<PostDto> showAllPosts() {
        List<Post> posts = postRepository.findAll();
        return posts.stream().map(this::mapFromPostToDto).collect(toList());
    }

    private PostDto mapFromPostToDto(Post post) {
        PostDto postDto = new PostDto();
        postDto.setId(post.getId());
        postDto.setTitle(post.getContent());
        postDto.setContent(post.getContent());
        postDto.setUsername(post.getUsername());
        return postDto;
    }
}
